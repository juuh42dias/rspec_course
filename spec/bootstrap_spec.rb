# require class
require_relative "../bootstrap"

# describe Class
RSpec.describe Bootstrap do 

  # expectation methods in class
  it "says hello" do
    expect(Bootstrap.new.hello).to include "Hello"
  end
end