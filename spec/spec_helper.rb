require_relative "../environment"
require_relative "../factories"
require "factory_girl"
require "database_cleaner"
require "rack/test"

RSpec.configure do |config|
 config.include FactoryGirl::Syntax::Methods

  config.before(:suite) do 
    begin 
      DatabaseCleaner.start
    ensure
      DatabaseCleaner.clean 
    end
  end
end