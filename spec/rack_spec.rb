require_relative "../environment"
require_relative "../rack_app"

RSpec.describe App do 
  include Rack::Test::Methods

  let(:app) { App }

  it "creates a book" do
    post :books, book: { name: "My first Book" }

    expect(last_response.status).to eq 201
  end
end