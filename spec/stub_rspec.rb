class Product
  attr_reader :price
end

class PriceCalculator
  def add product
    products << product
  end

  def products
  @product ||= []    
  end

  def final_price
    @product.map(&:price).inject(&:+ )
  end

end

RSpec.describe "Stubs" do

  let(:calculator) { PriceCalculator.new } 
  it "previous stubs to simulate state" do
    calculator = PriceCalculator.new
    product_stub = instance_double("Product")
    allow(product_stub).to receive(:price).and_return(1.0, 180.25)
    # allow(product_stub).to receive(:price) { 1.0 }
    
    2.times { calculator.add product_stub }

    expect(calculator.final_price).to  ex 181.25
  end

  it "provides mocks to assert on message passing" do
    # product_mock = double(:product)
    # expect(product_mock).to receive(:price).with(any_args)
    allow_any_instance_of(Product).to receive(:price).and_return(5)

    product = Product.new
    puts product.price

  end
end